package com.arun.aashu.firstuseofintent;

import android.content.Intent;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SeondActivity extends AppCompatActivity {

    TextView t;
    Button btn, btn2, btn3, btn4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seond);

        t = findViewById(R.id.textview2);
        btn = findViewById(R.id.whats);
        btn2 = findViewById(R.id.contacts);
        btn3 = findViewById(R.id.buttonLast);
        btn4 = findViewById(R.id.happnbutton);

        final Bundle b = getIntent().getExtras();

        String name2 = b.getString("name_key");
        String phone2 = b.getString("phone_key");

        t.setText(name2 + "\n" + phone2);


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in1 = getPackageManager().getLaunchIntentForPackage("com.whatsapp");
                startActivity(in1);
            }
        });


        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Intent.ACTION_VIEW
                        , ContactsContract.Contacts.CONTENT_URI);
                startActivity(intent);

            }
        });


        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in1 = new Intent(SeondActivity.this, MainActivity.class);
                startActivity(in1);
            }
        });

        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(android.content.Intent.ACTION_VIEW);
                i.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.ftw_and_co.happn"));
                startActivity(i);
            }
        });

    }

}