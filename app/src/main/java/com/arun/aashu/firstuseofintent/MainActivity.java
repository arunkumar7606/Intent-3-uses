package com.arun.aashu.firstuseofintent;

import android.content.Intent;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class MainActivity extends AppCompatActivity {

    Button b, b2, b3;
    EditText e1, e2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b = findViewById(R.id.button1);
        e1 = findViewById(R.id.edittex11);
        e2 = findViewById(R.id.edittext2);

        b2 = findViewById(R.id.dialer);
        b3 = findViewById(R.id.camera);


        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String name = e1.getText().toString();
                String phone = e2.getText().toString();

                Intent i = new Intent(MainActivity.this, SeondActivity.class);
                i.putExtra("name_key", name);
                i.putExtra("phone_key", phone);
                startActivity(i);
            }
        });


        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                startActivity(intent);
            }
        });

        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivity(in);
            }
        });


    }
}
